#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  COGniz: COG annotation of CDS                                                                             #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2024 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
#            4888888883                                                                                      #
#         48800007   4003 1                                                                                  #
#      4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888    #
#     4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181       #
#    40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181       #
#   100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181       #
#   81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101       #
#   808883     1    887  6000008                                                                             #
#   8000000003         480000008                                                                             #
#   600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000     #
#    60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39    #
#     6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007     #
#      680000008    800000087        180       40000000  4     109    181    10        68    87  10   06     #
#        6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00    #
#            688    8887                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.1;                                                                                               #
# + adjusted to DIAMOND v2.1.9                                                                               #
#                                                                                                            #
# VERSION=1.0;                                                                                               #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = CONSTANTS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- n/a --------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
# -- GenBank key words ------------------------------------------------------------------------------------  #
#                                                                                                            #
  KEY_LOCUS="/locus_tag=";
  KEY_PROID="/protein_id=";
  KEY_TRANS="/translation=";
#                                                                                                            #
# -- COG category descriptions ----------------------------------------------------------------------------  #
#                                                                                                            #
  # Information storage and processing
  JCOGD="Translation, ribosomal structure and biogenesis";
  ACOGD="RNA processing and modification";
  KCOGD="Transcription";
  LCOGD="Replication, recombination and repair";
  BCOGD="Chromatin structure and dynamics";
  # Cell processing and signaling
  DCOGD="Cell cycle control, cell division, chromosome partitioning";
  YCOGD="Nuclear structure";
  VCOGD="Defense mechanisms";
  TCOGD="Signal transduction mechanisms";
  MCOGD="Cell wall/membrane/envelope biogenesis";
  NCOGD="Cell motility";
  ZCOGD="Cytoskeleton";
  WCOGD="Extracellular structures";
  UCOGD="Intracellular trafficking, secretion, and vesicular transport";
  OCOGD="Posttranslational modification, protein turnover, chaperones";
  XCOGD="Mobilome: prophages, transposons";
  # Metabolism
  CCOGD="Energy production and conversion";
  GCOGD="Carbohydrate transport and metabolism";
  ECOGD="Amino acid transport and metabolism";
  FCOGD="Nucleotide transport and metabolism";
  HCOGD="Coenzyme transport and metabolism";
  ICOGD="Lipid transport and metabolism";
  PCOGD="Inorganic ion transport and metabolism";
  QCOGD="Secondary metabolites biosynthesis, transport and catabolism";
  # poorly characterized
  RCOGD="General function prediction only";
  SCOGD="Function unknown";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# - SUMMARY -                                                                                                #
# gawk/5.0.1 diamond/2.0.15 
#                                                                                                            #
# -- gawk >4.0 --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
#                                                                                                            #
# -- diamond >2.0 -----------------------------------------------------------------------------------------  #
#                                                                                                            #
  DIAMOND_BIN=diamond;
#                                                                                                            #
# -- GNU coreutils and other tools ------------------------------------------------------------------------  #
#                                                                                                            #
# echo  join  mktemp  paste  printf  sort  tr                                                                #
# bc  grep  sed                                                                                              #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = FITTINGS     =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BAWK="$GAWK_BIN";
  TAWK="$GAWK_BIN -F\\t";
#                                                                                                            #
# -- diamond ----------------------------------------------------------------------------------------------  #
#                                                                                                            #
  DIAMAKEDB="$DIAMOND_BIN makedb --quiet ";
  DIAGETSEQ="$DIAMOND_BIN getseq";
  DSEARCH="--masking none --max-target-seqs 10 --max-hsps 10 --min-score 20";
  DMEMORY="--block-size 5.0 --index-chunks 1";
  DIABLASTP="$DIAMOND_BIN blastp $DMEMORY $DSEARCH --quiet";  
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = DOC      =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m COGniz v$VERSION                                          $COPYRIGHT\033[0m";
  cat <<EOF

 https://gitlab.pasteur.fr/GIPhy/COGniz

 USAGE:  COGniz  -i <infile>   [-d <COGdb>]     [-o <outfile>]  [-O <outfile>] 
                [-w <tmpdir>]  [-t <nthreads>]  [-v]  [-c]  [-h]
 OPTIONS:
  -i <file>    input file name in FASTA or GenBank format (mandatory)
  -d <file>    COG databank file (default: db/COGdb.dmnd in the same directory as COGniz)
  -o <string>  output file name to write COG assignment results (default: none)
  -O <string>  output file name to  write the input file  completed with  COG assignments
               (default: none)
  -w <dir>     path to the tmp directory (default: \$TMPDIR, otherwise /tmp)
  -t <int>     thread numbers (default: 2)
  -v           verbose mode
  -c           checks dependencies and exit
  -h           prints this help and exit

EOF
} 
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = FUNCTIONS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- echoxit() --------------------------------------------------------------------------------------------- #
# >> prints in stderr the specified error message $1 and next exit 1                                         #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# -- chrono -----------------------------------------------------------------------------------------------  #
# >> returns the elapsed time in well-formatted format                                                       #
#                                                                                                            #
chrono() {
  local s=$SECONDS; printf "[%02d:%02d]" $(( $s / 60 )) $(( $s % 60 )) ;
}
#                                                                                                            #
# -- dcontrol ---------------------------------------------------------------------------------------------  #
# >> controls required and optional (non-coreutils) dependancies                                             #
#                                                                                                            #
dcontrol() {
  fail=false;
  for binexe in  bc  grep  sed  $GAWK_BIN  $DIAMOND_BIN
  do
    if [ ! $(command -v $binexe) ]; then echo "[ERROR] $binexe not found" >&2 ; fail=true; fi
  done
  if [ -z "$($DIAMOND_BIN version | $BAWK '{print$3}' | $BAWK -F"." '{print $1"."$2}' | $BAWK '($1>=2.0)')" ]
  then echo "[ERROR] $DIAMOND_BIN version < 2.0" >&2 ; fail=true ; fi
  if $fail ; then exit 1 ; fi
}
#                                                                                                            #
# -- dcheck -----------------------------------------------------------------------------------------------  #
# >> checks (non-coreutils) dependancies and exit                                                            #
#                                                                                                            #
dcheck() {
  echo "Checking required dependencies ..." ;
  ## bc ##############################
  echo -e -n "> \e[1mbc\e[0m          >1.0       mandatory\t" ;       binexe=bc;
  echo -e -n "$binexe      \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   bc -v | head -1 ;
  fi
  ## grep ############################
  echo -e -n "> \e[1mgrep\e[0m        >1.0       mandatory\t" ;       binexe=grep;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   grep -V | head -1 ;
  fi
  ## sed #############################
  echo -e -n "> \e[1msed\e[0m         >1.0       mandatory\t" ;       binexe=sed;
  echo -e -n "$binexe     \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   sed --version | head -1 ;
  fi
  ## gawk >=4.0.0 ####################
  echo -e -n "> \e[1mgawk\e[0m        >4.0.0     mandatory\t" ;       binexe=$GAWK_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $GAWK_BIN -V | head -1 ;
  fi
  ## diamond >2.1 ####################
  echo -e -n "> \e[1mdiamond\e[0m     >2.1       mandatory\t" ;       binexe=$DIAMOND_BIN;
  echo -e -n "$binexe\t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $DIAMOND_BIN version ;
  fi
  echo "[exit]" ;
  exit ;
}
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# READING OPTIONS                                                                                            #
#                                                                                                            #
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

COGDB_DIR=${COGDB_DIR:-$PWD/db}
COGDB=$COGDB_DIR/COGdb.dmnd; # COG databank   -d
INFILE="$NA";                # infile         -i
OUTFILE="$NA";               # outfile        -o
RWRFILE="$NA";               # rwr infile     -O
TMP_DIR=${TMPDIR:-/tmp};     # tmp directory  -w
NTHREADS=2;                  # no. threads    -t
VERBOSE=false;               # verbose mode   -v        
DEBUG=false;                 # debug mode     -X

while getopts d:i:o:O:w:t:vchX option
do
  case $option in
  d)  COGDB="$OPTARG"    ;;
  i)  INFILE="$OPTARG"   ;;
  o)  OUTFILE="$OPTARG"  ;;
  O)  RWRFILE="$OPTARG"  ;;
  w)  TMP_DIR="$OPTARG"  ;;
  t)  NTHREADS=$OPTARG   ;;
  v)  VERBOSE=true       ;;
  c)  dcheck             ;;
  X)  DEBUG=true         ;;
  h)  mandoc ;  exit 0   ;;
  \?) mandoc ;  exit 1   ;;
  esac
done

$DEBUG && VERBOSE=true;

echo "# COGniz v$VERSION" ;
echo "# $COPYRIGHT" ;
echo "+ https://gitlab.pasteur.fr/GIPhy/COGniz" ;
if $VERBOSE
then
  echo "> Syst:  $MACHTYPE" ;
  echo "> Bash:  $BASH_VERSION" ;
  echo "> COGdb: $COGDB" ;
fi


##############################################################################################################
#                                                                                                            #
# CHECKING DEPENDANCIES AND OPTIONS                                                                          #
#                                                                                                            #
##############################################################################################################
dcontrol;

[ ! -e $COGDB ]                     && echoxit "[ERROR] databank not found (option -d): $COGDB" ;
[   -d $COGDB ]                     && echoxit "[ERROR] not a file (option -d): $COGDB" ;
[ ! -s $COGDB ]                     && echoxit "[ERROR] empty file (option -d): $COGDB" ;
[ ! -r $COGDB ]                     && echoxit "[ERROR] no read permission (option -d): $COGDB" ;

[ "$INFILE" == "$NA" ]              && echoxit "[ERROR] no input file (option -i)" ;
[ ! -e $INFILE ]                    && echoxit "[ERROR] input file not found (option -i): $INFILE" ;
[   -d $INFILE ]                    && echoxit "[ERROR] not a file (option -i): $INFILE" ;
[ ! -s $INFILE ]                    && echoxit "[ERROR] empty file (option -i): $INFILE" ;
[ ! -r $INFILE ]                    && echoxit "[ERROR] no read permission (option -i): $INFILE" ;

INFORMAT="$NA";
line="$(grep -v "^$" $INFILE | head -1)";
if   [ "$($BAWK '{print$1}' <<<"$line")" == "LOCUS" ]
then
  nl=$(grep -c -F "$KEY_LOCUS" $INFILE); 
  np=$(grep -c -F "$KEY_PROID" $INFILE); 
  nt=$(grep -c -F "$KEY_TRANS" $INFILE);
  if   [ $np -ne 0 ]; then             INFORMAT="GBP";
  elif [ $nl -ne 0 ]; then             INFORMAT="GBL";
  else                                 echoxit "[ERROR] neither locus tag nor protein id found in the GenBank input file (option -i): $INFILE" ;
  fi
  [ $nt -lt 10 ]                    && echoxit "[ERROR] no translation found in the GenBank input file (option -i): $INFILE" ;
elif [ "${line:0:1}" == ">" ]; then    INFORMAT="FA";
fi
[ "$INFORMAT" == "$NA" ]            && echoxit "[ERROR] unknown file format (option -i): $INFILE" ;

[[ $NTHREADS =~ ^[0-9]+$ ]]         || echoxit "[ERROR] incorrect value (option -t): $NTHREADS" ; 
 [ $NTHREADS -lt 1 ] && NTHREADS=1;


##############################################################################################################
#                                                                                                            #
# CREATING TMP DIRECTORY, DEFINING TRAP AND TMP FILES                                                        #
#                                                                                                            #
##############################################################################################################
MESSAGE="creating tmp directory" ;
echo -n "$(chrono) $MESSAGE ." ;

[ "${TMP_DIR:0:1}" != "/" ] && TMP_DIR="$(pwd)/$TMP_DIR";
if [ ! -e $TMP_DIR ]; then echo ; echo "[ERROR] tmp directory does not exist (option -w): $TMP_DIR" >&2 ; exit 1 ; fi
if [ ! -d $TMP_DIR ]; then echo ; echo "[ERROR] not a directory (option -w): $TMP_DIR"              >&2 ; exit 1 ; fi
if [ ! -w $TMP_DIR ]; then echo ; echo "[ERROR] no write permission (option -w): $TMP_DIR"          >&2 ; exit 1 ; fi

echo -n "." ;

TMP_DIR=$(mktemp -d -p $TMP_DIR COGniz.XXXXXXXXXX);
finalize() { rm -r $TMP_DIR ; }
trap 'finalize ; exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;

DIAMAKEDB="$DIAMAKEDB --threads $NTHREADS --tmpdir $TMP_DIR";
DIAGETSEQ="$DIAGETSEQ --threads $NTHREADS --tmpdir $TMP_DIR";
DIABLASTP="$DIABLASTP --threads $NTHREADS --tmpdir $TMP_DIR";  

echo -n "." ;

INSEQ=$TMP_DIR/seq.faa;       # CDS from INFILE
INDB=$TMP_DIR/seq.dmnd;       # formatted CDS from INFILE
ICBP=$TMP_DIR/icbp.tsv;       # blastp results (CDS against COG)
ICSRT=$TMP_DIR/icbp.srt.tsv;  # sorted blastp results (CDS against COG)
COGSEQ=$TMP_DIR/cog.faa;      # CDS from COG
CIBP=$TMP_DIR/cibp.tsv;       # blastp results (COG against CDS)
CISRT=$TMP_DIR/cibp.srt.tsv;  # sorted blastp results (CCOG against CDS)
RBH=$TMP_DIR/rbh.tsv;         # RBH results
RESULTS=$TMP_DIR/results.tsv; # all results
FTMP=$TMP_DIR/tmp.txt;        # tmp file

echo " [ok]" ;

if $VERBOSE ; then echo "> TMP_DIR=$TMP_DIR" ; fi


##############################################################################################################
#                                                                                                            #
# PROCESSING INFILE                                                                                          #
#                                                                                                            #
##############################################################################################################
MESSAGE="reading infile" ;
echo -n "$(chrono) $MESSAGE .." ;

if [ "$INFORMAT" == "FA" ]
then
  $BAWK '!/^>/  {s=s$0;next}
         (s!=""){print s;s=""}
                {print}
         END    {print s}' $INFILE > $INSEQ ;
else
  if [ "$INFORMAT" == "GBL" ]
  then k="$KEY_LOCUS"; # each CDS is identified using its locus tag
  else k="$KEY_PROID"; # each CDS is identified using its protein id
  fi
  t="$KEY_TRANS";
  $BAWK -v k="$k" -v t=$"$t" 'function extract(line){x=index(line,"=");
                                                     info=substr(line,x+1);
                                                     gsub("\"","",info);
                                                     return info;
                                                    }
                              BEGIN        {readseq=0}
                              (index($1,k)){key=extract($1); next}
                              (index($1,t)){seq=extract($1); 
                                            if(gsub("\"","\"",$1)!=2){readseq=1; next}
                                            print">"key; print seq;
                                            seq=""; readseq=0; next;
                                           }
                              (readseq)    {seq=seq""$1;
                                            if(index($1,"\"")==0) next
                                            gsub("\"","",seq); gsub(" ","",seq);
                                            print">"key; print seq;
                                            seq=""; readseq=0;
                                           }' $INFILE > $INSEQ ;
fi

NSEQ=$(grep -c "^>" $INSEQ);

echo -n "." ;

$DIAMAKEDB --in $INSEQ --db $INDB ;

echo " [ok]" ;

if $VERBOSE
then
                              echo "> file name:       $INFILE" ;
  [ "$INFORMAT" == "FA"  ] && echo "> detected format: FASTA" ;
  [ "$INFORMAT" == "GBL" ] && echo "> detected format: GenBank (with locus tags)" ;
  [ "$INFORMAT" == "GBP" ] && echo "> detected format: GenBank (with protein ids)" ;
                              echo "> no. CDS:         $NSEQ" ;
fi    


##############################################################################################################
#                                                                                                            #
# SEARCHING COG DATABANK                                                                                     #
#                                                                                                            #
##############################################################################################################
MESSAGE="searching COG databank" ;
echo -n "$(chrono) $MESSAGE ..." ;

$DIABLASTP --iterate --query $INSEQ --db $COGDB --outfmt 6 qseqid sseqid bitscore > $FTMP ;

echo -n "." ;

### first assignment: 10 FBHs with bit score > 60 and identical COG family
$TAWK '($3>60)' $FTMP |                  ## bit score at least 60
  sort -k1,1 -k3,3rg |                   ## sorting by query and next by bit score
    tr ':' '\t' |
      $TAWK '($1!=q){if(n>=10)print l;
                     q=$1;
                     c=$2;
                     l=$0;
                     n=1;
                     next;
                    }
             ($2==c){++n}' > $RESULTS ;  ## saving each query for which the 10 FBHs belong to the same COG family

echo -n "." ;

### gathering results for unassigned queries
$TAWK '(NR==FNR){q[$1];next}
       ($1 in q){next}
                {print}' $RESULTS $FTMP |
  sort -k1,1 -k3,3rg |                   ## sorting by query and next by bit score
    $TAWK '($1!=q){print;
                   q=$1; b=$3;
                   next;
                  }
           ($3==b){print}' > $ICBP ;     ## getting the best hit(s) by query (not necessarily unique)

echo -n "." ;

### gathering COG sequences associated to the unassigned queries
$TAWK '{print$2}' $ICBP > $FTMP ;
$DIAGETSEQ --db $COGDB |
  paste - - |
    grep -F -f $FTMP |
      tr '\t' '\n' > $COGSEQ ;
rm -f $FTMP ;

echo " [ok]" ;

NASS=$(cat $RESULTS | wc -l);

if $VERBOSE
then
  echo "> no. assigned CDS:   $NASS" ;
  echo "> no. unassigned CDS: $(( NSEQ - $NASS ))" ;
fi


##############################################################################################################
#                                                                                                            #
# RECIPROCAL SEARCHING                                                                                       #
#                                                                                                            #
##############################################################################################################
MESSAGE="reciprocal searching" ;
echo -n "$(chrono) $MESSAGE ." ;

$DIABLASTP --sensitive --query-cover 50 --query $COGSEQ --db $INDB --outfmt 6 qseqid sseqid bitscore |
  sort -k1,1 -k3,3rg |                ## sorting by query and next by bit score
    $TAWK '($1!=q){print;
                   q=$1; b=$3;
                   next;
                  }
           ($3==b){print}' > $CIBP ;  ## getting the best hit(s) by query (not necessarily unique)

echo -n "." ;

sort -k1 $ICBP > $ICSRT &
sort -k2 $CIBP > $CISRT ;

wait ;

echo -n "." ;

join -t$'\t' -1 1 -2 2 $ICSRT $CISRT |
  $TAWK '($2==$4){print $1"\t"$2"\t"$3}' |
    tr ':' '\t' |
      sort |
        $TAWK '($1!=q){print;q=$1}' > $RBH ;

echo " [ok]" ;

NRBH=$(cat $RBH | wc -l);
cat $RBH >> $RESULTS ;
NASS=$(cat $RESULTS | wc -l);

if $VERBOSE
then
  echo "> no. RBHs:           $NRBH" ;
  echo "> no. assigned CDS:   $NASS" ;
  echo "> no. unassigned CDS: $(( NSEQ - $NASS ))" ;
fi

##############################################################################################################
#                                                                                                            #
# DISPLAYING STATS                                                                                           #
#                                                                                                            #
##############################################################################################################

$BAWK '{print$3}' $RESULTS |
  grep -o "." |
    $BAWK 'BEGIN{cog["J"]=0; cog["A"]=0; cog["K"]=0; 
                 cog["L"]=0; cog["B"]=0; cog["D"]=0; 
                 cog["Y"]=0; cog["V"]=0; cog["T"]=0; 
                 cog["M"]=0; cog["N"]=0; cog["Z"]=0; 
                 cog["W"]=0; cog["U"]=0; cog["O"]=0; 
                 cog["X"]=0; cog["C"]=0; cog["G"]=0; 
                 cog["E"]=0; cog["F"]=0; cog["H"]=0; 
                 cog["I"]=0; cog["P"]=0; cog["Q"]=0; 
                 cog["R"]=0; cog["S"]=0; 
                }
                {cog[$1]++}
           END  {for (c in cog) print c"\t"cog[c]}' > $FTMP ;

JCOG=$(grep "^J" $FTMP | $TAWK '{print $2}');
ACOG=$(grep "^A" $FTMP | $TAWK '{print $2}'); 
KCOG=$(grep "^K" $FTMP | $TAWK '{print $2}'); 
LCOG=$(grep "^L" $FTMP | $TAWK '{print $2}'); 
BCOG=$(grep "^B" $FTMP | $TAWK '{print $2}'); 
DCOG=$(grep "^D" $FTMP | $TAWK '{print $2}'); 
YCOG=$(grep "^Y" $FTMP | $TAWK '{print $2}'); 
VCOG=$(grep "^V" $FTMP | $TAWK '{print $2}'); 
TCOG=$(grep "^T" $FTMP | $TAWK '{print $2}'); 
MCOG=$(grep "^M" $FTMP | $TAWK '{print $2}'); 
NCOG=$(grep "^N" $FTMP | $TAWK '{print $2}'); 
ZCOG=$(grep "^Z" $FTMP | $TAWK '{print $2}'); 
WCOG=$(grep "^W" $FTMP | $TAWK '{print $2}'); 
UCOG=$(grep "^U" $FTMP | $TAWK '{print $2}'); 
OCOG=$(grep "^O" $FTMP | $TAWK '{print $2}'); 
XCOG=$(grep "^X" $FTMP | $TAWK '{print $2}'); 
CCOG=$(grep "^C" $FTMP | $TAWK '{print $2}'); 
GCOG=$(grep "^G" $FTMP | $TAWK '{print $2}'); 
ECOG=$(grep "^E" $FTMP | $TAWK '{print $2}'); 
FCOG=$(grep "^F" $FTMP | $TAWK '{print $2}'); 
HCOG=$(grep "^H" $FTMP | $TAWK '{print $2}'); 
ICOG=$(grep "^I" $FTMP | $TAWK '{print $2}'); 
PCOG=$(grep "^P" $FTMP | $TAWK '{print $2}'); 
QCOG=$(grep "^Q" $FTMP | $TAWK '{print $2}'); 
RCOG=$(grep "^R" $FTMP | $TAWK '{print $2}'); 
SCOG=$(grep "^S" $FTMP | $TAWK '{print $2}');
NONE=$(( $NSEQ - $NASS ));

echo ;

echo -e "ctg\tno.\tdescription" ;
echo -e "---\t---\t-------------------------------------------------------------" ;

#### Information storage and processing
echo -e "J\t$JCOG\t\e[45m\e[97m$JCOGD\e[0m" ; # FCCCFC
echo -e "A\t$ACOG\t\e[45m\e[97m$ACOGD\e[0m" ; # FCDCFC
echo -e "K\t$KCOG\t\e[45m\e[97m$KCOGD\e[0m" ; # FCDCEC
echo -e "L\t$LCOG\t\e[45m\e[97m$LCOGD\e[0m" ; # FCDCDC
echo -e "B\t$BCOG\t\e[45m\e[97m$BCOGD\e[0m" ; # FCDCCC

### Cell processing and signaling
echo -e "D\t$DCOG\t\e[43m\e[97m$DCOGD\e[0m" ; # FCFCDC
echo -e "Y\t$YCOG\t\e[43m\e[97m$YCOGD\e[0m" ; # FCFCCC
echo -e "V\t$VCOG\t\e[43m\e[97m$VCOGD\e[0m" ; # FCFCBC
echo -e "T\t$TCOG\t\e[43m\e[97m$TCOGD\e[0m" ; # FCFCAC

echo -e "M\t$MCOG\t\e[42m\e[97m$MCOGD\e[0m" ; # ECFCAC
echo -e "N\t$NCOG\t\e[42m\e[97m$NCOGD\e[0m" ; # DCFCAC
echo -e "Z\t$ZCOG\t\e[42m\e[97m$ZCOGD\e[0m" ; # CCFCAC
echo -e "W\t$WCOG\t\e[42m\e[97m$WCOGD\e[0m" ; # BCFCAC
echo -e "U\t$UCOG\t\e[42m\e[97m$UCOGD\e[0m" ; # ACFCAC
echo -e "O\t$OCOG\t\e[42m\e[97m$OCOGD\e[0m" ; # 9CFCAC
echo -e "X\t$XCOG\t\e[42m\e[97m$XCOGD\e[0m" ; # 9CFC9C

### Metabolism
echo -e "C\t$CCOG\t\e[46m\e[97m$CCOGD\e[0m" ; # BCFCFC
echo -e "G\t$GCOG\t\e[46m\e[97m$GCOGD\e[0m" ; # CCFCFC
echo -e "E\t$ECOG\t\e[46m\e[97m$ECOGD\e[0m" ; # DCFCFC
echo -e "F\t$FCOG\t\e[46m\e[97m$FCOGD\e[0m" ; # DCECFC
echo -e "H\t$HCOG\t\e[46m\e[97m$HCOGD\e[0m" ; # DCDCFC
echo -e "I\t$ICOG\t\e[46m\e[97m$ICOGD\e[0m" ; # DCCCFC
echo -e "P\t$PCOG\t\e[46m\e[97m$PCOGD\e[0m" ; # CCCCFC
echo -e "Q\t$QCOG\t\e[46m\e[97m$QCOGD\e[0m" ; # BCCCFC

### poorly characterized
echo -e "R\t$RCOG\t\e[100m\e[97m$RCOGD\e[0m" ; # E0E0E0
echo -e "S\t$SCOG\t\e[100m\e[97m$SCOGD\e[0m" ; # CCCCCC

### non assigned
echo -e "-\t$NONE\t\e[40m\e[97mCDS without assigned COG\e[0m" ;

echo ;


##############################################################################################################
#                                                                                                            #
# WRITING OUTPUT FILE(S)                                                                                     #
#                                                                                                            #
##############################################################################################################
if [ "$OUTFILE" != "$NA" ] || [ "$RWRFILE" != "$NA" ] 
then
  MESSAGE="writing output file(s)" ;
  echo -n "$(chrono) $MESSAGE .." ;
 
  if [ "$OUTFILE" != "$NA" ]
  then
    { echo -e "#file\tnCDS\tnASS" ;
      echo -e "$INFILE\t$NSEQ\t$NASS" ;

      echo ;

      echo -e "#ctg\tno.\tdescription" ;
      #### Information storage and processing
      echo -e "J\t$JCOG\t$JCOGD" ;
      echo -e "A\t$ACOG\t$ACOGD" ;
      echo -e "K\t$KCOG\t$KCOGD" ;
      echo -e "L\t$LCOG\t$LCOGD" ;
      echo -e "B\t$BCOG\t$BCOGD" ;
      ### Cell processing and signaling
      echo -e "D\t$DCOG\t$DCOGD" ;
      echo -e "Y\t$YCOG\t$YCOGD" ;
      echo -e "V\t$VCOG\t$VCOGD" ;
      echo -e "T\t$TCOG\t$TCOGD" ;
      echo -e "M\t$MCOG\t$MCOGD" ;
      echo -e "N\t$NCOG\t$NCOGD" ;
      echo -e "Z\t$ZCOG\t$ZCOGD" ;
      echo -e "W\t$WCOG\t$WCOGD" ;
      echo -e "U\t$UCOG\t$UCOGD" ;
      echo -e "O\t$OCOG\t$OCOGD" ;
      echo -e "X\t$XCOG\t$XCOGD" ;
      ### Metabolism
      echo -e "C\t$CCOG\t$CCOGD" ;
      echo -e "G\t$GCOG\t$GCOGD" ;
      echo -e "E\t$ECOG\t$ECOGD" ;
      echo -e "F\t$FCOG\t$FCOGD" ;
      echo -e "H\t$HCOG\t$HCOGD" ;
      echo -e "I\t$ICOG\t$ICOGD" ;
      echo -e "P\t$PCOG\t$PCOGD" ;
      echo -e "Q\t$QCOG\t$QCOGD" ;
      ### poorly characterized
      echo -e "R\t$RCOG\t$RCOGD" ;
      echo -e "S\t$SCOG\t$SCOGD" ;

      echo ;

      echo -e "#accn\tCOG.id\tCOG.ctg\tCOG.seq\tscore" ;
      sed -n 1~2p $INSEQ | tr -d '>' > $FTMP ;
      $BAWK '(NR==FNR)   {info[$1]=$0;next}
             ($1 in info){print info[$1];next}
                         {print $1"\t-\t-\t-\t-"}' $RESULTS $FTMP ;
    } > $OUTFILE ;
  fi

  echo -n "." ;

  if [ "$RWRFILE" != "$NA" ]
  then
    { case "$INFORMAT" in
      # FASTA output
      FA)  $BAWK '(NR==FNR){cog[$1]=$2;
                            ctg[$1]=$3;
                            next;
                           }
                  /^>/     {key=substr($1,2); 
                            if (key in cog) print $0" @ "cog[key]":"ctg[key];
                            else            print $0;
                            next;
                           }
                           {print}' $RESULTS $INFILE ;;  
      # GenBank output (based on locug tags)
      GBL) k="$KEY_LOCUS";
           $BAWK -v k="$k" 'function extract(line){x=index(line,"=");
                                                   info=substr(line,x+1);
                                                   gsub("\"","",info);
                                                   return info;
                                                  }
                            BEGIN        {PRE="                     /note=";
                                          SUF=" inferred using COGniz\"";
                                         }
                            (NR==FNR)    {cog[$1]=$2;
                                          ctg[$1]=$3;
                                          next;
                                         }
                            ($1=="CDS")  {cds=1}
                            ($1=="gene") {cds=0}
                            (cds==0)     {print;
                                          next;
                                         }
                            (index($1,k)){i=extract($1);
                                          if (i in cog) print PRE"\""cog[i]":"ctg[i]";"SUF;
                                         }
                                         {print}' $RESULTS $INFILE ;;
      # GenBank output (based on protein ids)
      GBP) k="$KEY_PROID";
           $BAWK -v k="$k" 'function extract(line){x=index(line,"=");
                                                   info=substr(line,x+1);
                                                   gsub("\"","",info);
                                                   return info;
                                                  }
                            BEGIN        {PRE="                     /note=";
                                          SUF=" inferred using COGniz\"";
                                         }
                            (NR==FNR)    {cog[$1]=$2;
                                          ctg[$1]=$3;
                                          next;
                                         }
                            (index($1,k)){i=extract($1);
                                          if (i in cog) print PRE"\""cog[i]":"ctg[i]";"SUF;
                                         }
                                         {print}' $RESULTS $INFILE ;;
      esac
    } > $RWRFILE ;
  fi

  echo " [ok]" ;

  if $VERBOSE
  then
    [ "$OUTFILE" != "$NA" ] && echo "+ COG assignments: $OUTFILE" ;
    [ "$RWRFILE" != "$NA" ] && echo "+ sequence file:   $RWRFILE" ;
  fi
fi

finalize ;

echo "$(chrono) exit" ;

exit ;


