#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  makeCOGdb: creating COG databank from NCBI to be used by COGniz                                           #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2024 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
#            4888888883                                                                                      #
#         48800007   4003 1                                                                                  #
#      4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888    #
#     4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181       #
#    40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181       #
#   100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181       #
#   81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101       #
#   808883     1    887  6000008                                                                             #
#   8000000003         480000008                                                                             #
#   600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000     #
#    60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39    #
#     6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007     #
#      680000008    800000087        180       40000000  4     109    181    10        68    87  10   06     #
#        6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00    #
#            688    8887                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.1;                                                                                               #
# + adjusted to DIAMOND v2.1.9                                                                               #
#                                                                                                            #
# VERSION=1.0;                                                                                               #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gzip | pigz ------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GZIP_BIN=gzip;
  PIGZ_BIN=pigz;
  GOT_PIGZ=false;
  if [ ! $(command -v $GZIP_BIN) ]; then echo "no $GZIP_BIN detected" >&2 ; exit 1 ; fi
  GZIP_STATIC_OPTIONS="--quiet --best";     
  GZIP="$GZIP_BIN $GZIP_STATIC_OPTIONS";
  GUNZIP="$GZIP --decompress";
  if [ $(command -v $PIGZ_BIN) ]
  then
    GOT_PIGZ=true;
    PIGZ_STATIC_OPTIONS="--quiet --best";     
    PIGZ="$PIGZ_BIN $PIGZ_STATIC_OPTIONS";
  fi
#                                                                                                            #
# -- wget | curl ------------------------------------------------------------------------------------------  #
#                                                                                                            #
  WGET_BIN=wget;
  CURL_BIN=curl;
  USE_WGET=true;
  if [ $(command -v $WGET_BIN) ]
  then
    WGET_STATIC_OPTIONS="--quiet --retry-connrefused --no-check-certificate";     
    WGET="nice $WGET_BIN $WGET_STATIC_OPTIONS";
  elif [ $(command -v $CURL_BIN) ]
  then
    USE_WGET=false;
    CURL_STATIC_OPTIONS="--silent --location --continue-at -";     
    CURL="nice $CURL_BIN $CURL_STATIC_OPTIONS";
  else
    echo "neither $WGET_BIN nor $CURL_BIN detected" >&2 ;
    exit 1 ;
  fi
#                                                                                                            #
# -- diamond version > 2.0 --------------------------------------------------------------------------------  #
#                                                                                                            #
  DIAMOND_BIN=diamond;
  if [ ! $(command -v $DIAMOND_BIN) ]; then echo "no $DIAMOND_BIN detected" >&2 ; exit 1 ; fi
  if [ -z "$($DIAMOND_BIN version | awk '{print$3}' | awk -F"." '{print $1"."$2}' | awk '($1>=2.0)')" ]
  then  echo "$DIAMOND_BIN version < 2.0" >&2 ; exit 1 ; fi
  DIAMAKEDB="$DIAMOND_BIN makedb --quiet ";
  DIAVERSION="$DIAMOND_BIN version";
#                                                                                                            #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = FUNCTIONS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# = dwnlCOG() arguments: =================================================================================   #
#    1. COGID:    COG id                                                                                     #
#    2. OUTDIR:   output directory                                                                           #
#    3. USE_WGET: whether to use WGET or CURL                                                                #
#   downloads the 2 COG files COGID.fa.gz and COGID.tsv.gz and uncompress them into OUTDIR                   #
#                                                                                                            #
dwnlCOG() {
  DIRURL="https://ftp.ncbi.nih.gov/pub/COG/COG2020/data/fasta";
  ftmp=$2/$1.tmp.gz;
  while [ 1 ]
  do
    if $3
    then $WGET -O $ftmp  $DIRURL/$1.fa.gz ;
    else $CURL -o $ftmp  $DIRURL/$1.fa.gz ;
    fi
    if [ $? == 0 ]; then $GUNZIP -c $ftmp > $2/$1.fa ; rm -f $ftmp ; break; fi
    sleep 1 ;
  done
  while [ 1 ]
  do
    if $3
    then $WGET -O $ftmp  $DIRURL/$1.tsv.gz ;
    else $CURL -o $ftmp  $DIRURL/$1.tsv.gz ;
    fi
    if [ $? == 0 ]; then $GUNZIP -c $ftmp > $2/$1.tsv ; rm -f $ftmp ; break; fi
    sleep 1 ;
  done
  echo "ok" > $2/$1.complete ;
  return 0 ;
}
#                                                                                                            #
# = dwnlCOGs() arguments: =================================================================================  #
#    1. INFILE:   COG description file                                                                       #
#    2. OUTDIR:   output directory                                                                           #
#    3. NTHREADS: no. threads                                                                                #
#    4. USE_WGET: whether to use WGET or CURL                                                                #
#   downloads all COG files from the specified COG description one                                           #
#                                                                                                            #
dwnlCOGs() {
  while read id _
  do
    dwnlCOG $id $2 $4 &
    while [ $(jobs -r | wc -l) -gt $3 ]; do sleep 0.5 ; done
  done < $1
  return 0 ;
}
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# INIT                                                                                                       #
#                                                                                                            #
##############################################################################################################
NTHREADS=6;
[ $# -gt 0 ] && NTHREADS=$1;
[[ $NTHREADS =~ ^[0-9]+$ ]] || NTHREADS=6;

TMP_DIR=$(mktemp -d -p $(pwd) XXXXXXXXXX);
finalize() { rm -f $TMP_DIR/* ; rm -rf $TMP_DIR ; }
trap 'sleep 1;finalize;exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;

echo "makeCOGdb v$VERSION $COPYRIGHT" ;
echo "no. threads: $NTHREADS" ;
echo "tmp dir: $TMP_DIR" ;

COGDB=$TMP_DIR/COGdb;

##############################################################################################################
#                                                                                                            #
# DOWNLOADING COG DESCRIPTION FILE                                                                           #
#                                                                                                            #
##############################################################################################################
FILEURL="https://ftp.ncbi.nih.gov/pub/COG/COG2020/data/cog-20.def.tab";
echo -n "downloading COG description ." ;
if $USE_WGET
then while true ; do echo -n "." ; $WGET -O $TMP_DIR/cog.tsv  $FILEURL  && break ; done
else while true ; do echo -n "." ; $CURL -o $TMP_DIR/cog.tsv  $FILEURL  && break ; done
fi
echo ". [ok]" ;

##############################################################################################################
#                                                                                                            #
# PROCESSING EACH DOWNLOADED COG FILES                                                                       #
#                                                                                                            #
##############################################################################################################

dwnlCOGs $TMP_DIR/cog.tsv $TMP_DIR $NTHREADS $USE_WGET &

sleep 1 ; 

while read cog ctg _
do
  echo -n "$cog ." ;

  log=$TMP_DIR/$cog.complete;
  faa=$TMP_DIR/$cog.fa;
  tsv=$TMP_DIR/$cog.tsv;

  while [ ! -s $log ]; do echo -n "." ; sleep 1 ; done
  rm -f $log ;

  echo -n "." ;

  # FASTA to sorted TSV: accn seq
  gawk '!/^>/  {s=s$0;next}
        (s!=""){print s;s=""}
               {print$1}
        END    {print s}' $faa | tr -d '>' | paste - - | sort > $faa.srt ;

  echo -n "." ;

  # TSV to sorted TSV: accn start end 
  sed 1d $tsv | cut -f1,5 | tr '=' '\t' | 
    gawk 'function lgt(range){i=index(range,"-"); s=substr(range,1,i); e=substr(range,i+1); return (e-s+1)}
          (NF==2){print;next}
                 {lmax=f=1;while(++f<=NF)if((l=lgt($f))>lmax){l=lmax;best=$f} print $1"\t"best}' | sort | tr '-' '\t' > $tsv.srt ;
  
  echo -n "." ;

  # joining the two sorted files and writing COG databank
  join -j 1 $tsv.srt $faa.srt |
    gawk -v cog=$cog -v ctg=$ctg '{print ">"cog":"ctg":"$1;
                                   print substr($4,$2,$3-$2+1);}' >> $COGDB ;

  echo -n "." ;

  # removing useless files
  rm -f $faa $faa.srt $tsv $tsv.srt ;
  
  echo ". [ok]" ;

done < $TMP_DIR/cog.tsv


##############################################################################################################
#                                                                                                            #
# FINALIZING                                                                                                 #
#                                                                                                            #
##############################################################################################################
echo -n "compressing and formatting .." ;

n=$(grep -c "^>" $COGDB);

echo -n "." ;

# if $GOT_PIGZ
# then $PIGZ -p $NTHREADS -c $COGDB > COGdb.gz ;
# else $GZIP              -c $COGDB > COGdb.gz ;
# fi
# 
# echo -n "." ;
# 
# $DIAMAKEDB --threads $NTHREADS --in COGdb.gz --db COGdb ;

$DIAMAKEDB --threads $NTHREADS --in $COGDB --db COGdb ;

echo -n "." ;

{ date +"%Y-%m-%d %H:%M:%S" ;
  echo "COGdb.dmnd" ;
  $DIAVERSION | sed 's/ersion //' ;
  echo "$n sequences" ;
  echo "built using makeCOGdb v$VERSION" ; } > COGdb.version.txt ;

finalize ;

echo ". [ok]" ;

echo ">> COG databank:"
# echo "   + FASTA:   COGdb.gz" ;
echo "   + diamond: COGdb.dmnd" ;
echo "   + version: COGdb.version.txt" ;
echo ">> no. sequences: $n" ;

exit ;
