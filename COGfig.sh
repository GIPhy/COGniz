#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  COGfig: creating figure from COGniz output file(s)                                                        #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2024 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
#            4888888883                                                                                      #
#         48800007   4003 1                                                                                  #
#      4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888    #
#     4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181       #
#    40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181       #
#   100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181       #
#   81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101       #
#   808883     1    887  6000008                                                                             #
#   8000000003         480000008                                                                             #
#   600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000     #
#    60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39    #
#     6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007     #
#      680000008    800000087        180       40000000  4     109    181    10        68    87  10   06     #
#        6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00    #
#            688    8887                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.1;                                                                                               #
# + updated usage                                                                                            #
#                                                                                                            #
# VERSION=1.0;                                                                                               #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = CONSTANTS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- svg variables ----------------------------------------------------------------------------------------  #
#                                                                                                            #
  FONTSIZE=6.5;
  FONTWIDTH=3.65;
#                                                                                                            # 
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# - SUMMARY -                                                                                                #
# gawk/5.0.1 librsvg/2.50.3
#                                                                                                            #
# -- gawk >4.0 --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
#                                                                                                            #
# -- rsvg-convert -----------------------------------------------------------------------------------------  #
#                                                                                                            #
  RSVGCONVERT_BIN=rsvg-convert;
#                                                                                                            #
# -- GNU coreutils and other tools ------------------------------------------------------------------------  #
#                                                                                                            #
# echo  sort                                                                                                 #
# bc  grep  sed                                                                                              #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = FITTINGS     =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BAWK="$GAWK_BIN";
  TAWK="$GAWK_BIN -F\\t";
#                                                                                                            #
# -- rsvg-convert -----------------------------------------------------------------------------------------  #
#                                                                                                            #
  SVG2PDF="$RSVGCONVERT_BIN --keep-aspect-ratio --dpi-x 300 --dpi-y 300 -f pdf";
  SVG2PNG="$RSVGCONVERT_BIN --keep-aspect-ratio --dpi-x 300 --dpi-y 300 -f png";
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = DOC      =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m COGfig v$VERSION                                       $COPYRIGHT\033[0m";
  cat <<EOF

 https://gitlab.pasteur.fr/GIPhy/COGniz

 USAGE:  COGfig  [options]  <infile>  [<infile> ...]

 OPTIONS:
  -o <string>  output file name; supported format: png, pdf, svg (default: COGfig.svg)
  -a           sort input files (ascending order of the no. assigned COGs)
  -d           sort input files (descending order of the no. assigned COGs)
  -v           verbose mode
  -h           prints this help and exit

EOF
} 
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# READING OPTIONS                                                                                            #
#                                                                                                            #
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

OUTFILE=COGfig.svg; # outfile         -o
ASORT=false;        # ascending sort  -a
DSORT=false;        # descending sort -d
VERBOSE=false;      # verbose mode    -v        
DEBUG=false;        # debug mode      -X

while getopts o:adhvX option
do
  case $option in
  o)  OUTFILE="$OPTARG"  ;;
  a)  ASORT=true         ;;
  d)  DSORT=true         ;;
  v)  VERBOSE=true       ;;
  X)  DEBUG=true         ;;
  h)  mandoc ;  exit 0   ;;
  \?) mandoc ;  exit 1   ;;
  esac
done

shift "$(( $OPTIND - 1 ))"
NFILE=$#;     # no. input file(s)
FLIST="$@";   # specified input file(s)

FEXT="${OUTFILE##*.}"
case $FEXT in
pdf) if [ ! $(command -v $RSVGCONVERT_BIN) ]; then echo "[ERROR] $RSVGCONVERT_BIN not found: unable to create pdf format" >&2 ; exit 1; fi ;;
png) if [ ! $(command -v $RSVGCONVERT_BIN) ]; then echo "[ERROR] $RSVGCONVERT_BIN not found: unable to create png format" >&2 ; exit 1; fi ;;
esac

FTMP=$(mktemp ${TMPDIR:-/tmp}/COGfig.XXXXXXXXXX);
finalize() { rm -f $FTMP ; }
trap 'finalize ; exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;

$DEBUG && VERBOSE=true;

echo "# COGfig v$VERSION" ;
echo "# $COPYRIGHT" ;
echo "+ https://gitlab.pasteur.fr/GIPhy/COGfig" ;
if $VERBOSE
then
  echo "> Syst:  $MACHTYPE" ;
  echo "> Bash:  $BASH_VERSION" ;
fi

if $ASORT || $DSORT
then
  for fin in $FLIST ; do echo -n -e "$fin\t" ; sed -n '2p;2q' $fin ; done > $FTMP ;
  if $ASORT ; then sort -nk4 $FTMP ; else sort -nrk4 $FTMP ; fi > $FTMP.srt ;
  line=""; while IFS=$'\t' read -r f _ ; do line="$line $f" ; done < $FTMP.srt
  FLIST="$line";
  rm -r $FTMP.srt  ;
fi

##############################################################################################################
#                                                                                                            #
# ONE INPUT FILE                                                                                             #
#                                                                                                            #
##############################################################################################################
if [ $NFILE -eq 1 ]
then
  fin=$FLIST;

  $VERBOSE && echo "> input file: $fin" ;

  # line="$(sed -n '2p;2q' $fin)";
  # fname=$($TAWK '{print$1}' <<<"$line");
  # ncds=$($TAWK '{print$2}' <<<"$line");
  # ncog=$($TAWK '{print$3}' <<<"$line");

  sed -n '5,30p;30q' $fin | 
    sed 's/^J\t/FCCCFC\tJ\t/;
         s/^A\t/FCDCFC\tA\t/;
         s/^K\t/FCDCEC\tK\t/;
         s/^L\t/FCDCDC\tL\t/;
         s/^B\t/FCDCCC\tB\t/;
         s/^D\t/FCFCDC\tD\t/;
         s/^Y\t/FCFCCC\tY\t/;
         s/^V\t/FCFCBC\tV\t/;
         s/^T\t/FCFCAC\tT\t/;
         s/^M\t/ECFCAC\tM\t/;
         s/^N\t/DCFCAC\tN\t/;
         s/^Z\t/CCFCAC\tZ\t/;
         s/^W\t/BCFCAC\tW\t/;
         s/^U\t/ACFCAC\tU\t/;
         s/^O\t/9CFCAC\tO\t/;
         s/^X\t/9CFC9C\tX\t/;
         s/^C\t/BCFCFC\tC\t/;
         s/^G\t/CCFCFC\tG\t/;
         s/^E\t/DCFCFC\tE\t/;
         s/^F\t/DCECFC\tF\t/;
         s/^H\t/DCDCFC\tH\t/;
         s/^I\t/DCCCFC\tI\t/;
         s/^P\t/CCCCFC\tP\t/;
         s/^Q\t/BCCCFC\tQ\t/;
         s/^R\t/E0E0E0\tR\t/;
         s/^S\t/CCCCCC\tS\t/' > $FTMP ;

  XMAX=$($TAWK '($3>max){max=$3}END{print max}' $FTMP);

  WIDTH=490;    # whole figure dimension
  HEIGTH=290;   # whole figure dimension

  HWIDTH=240;   # width of the histogram
  HHEIGTH=257;  # heigth of the histogram
  HXSTART=220;  # x start of the histogram
  HYSTART=20;   # y start of the histogram

  case $FEXT in # base color of the histogram
  pdf) HCOLOR="grey"          ;;
  png) HCOLOR="darkslategrey" ;; 
  svg) HCOLOR="darkslategrey" ;;
  esac

  GSCALE=$(bc -l <<<"scale=3;2000/$WIDTH" | sed 's/^\./0\./');
  GWIDTH=$(bc <<<"($WIDTH*$GSCALE+0.5)/1");
  GHEIGTH=$(bc <<<"($HEIGTH*$GSCALE+0.5)/1");

  { echo -e "<svg version=\"1.1\" width=\"$GWIDTH\" height=\"$GHEIGTH\" xmlns=\"http://www.w3.org/2000/svg\">\n" ;

    echo -e " <style>" ;
    echo -e "  text {" ;
    echo -e "   font-family: Arial, Helvetica, Liberation, sans-serif;" ;
    echo -e "   fill: black;" ;
    echo -e "  }" ;
    echo -e "  line {" ;
    echo -e "   stroke-linecap: round;" ;
    echo -e "  }" ;
    echo -e " </style>\n" ;
  
    echo -e " <g transform=\"scale($GSCALE)\">\n" ;

    echo -e "  <!-- background -->\n" ;

    echo -e "  <rect width=\"$WIDTH\" height=\"$HEIGTH\" fill=\"white\"/>\n" ;

    echo -e "  <!-- COG categories -->\n" ;

    y=$(( $HYSTART + 7 - 10 ));
    while IFS=$'\t' read -r hex ctg card description
    do
      x=$(( $HXSTART - 20 ));
      y=$(( $y + 10 ));
      echo -e "  <text x=\"$x\" y=\"$y\" font-size=\"$FONTSIZE\" text-anchor=\"end\">$description</text>\n";
      x=$(( $HXSTART - 10 ));
      echo -e "  <text x=\"$x\" y=\"$y\" font-size=\"$FONTSIZE\" text-anchor=\"middle\">[$ctg]</text>\n";
    done < $FTMP ;
      
    echo -e "  <!-- y-axis -->\n" ;

    step=2;
    [ $XMAX -gt 10 ]    && step=5;
    [ $XMAX -gt 50 ]    && step=10;
    [ $XMAX -gt 100 ]   && step=20;
    [ $XMAX -gt 500 ]   && step=100;
    [ $XMAX -gt 1000 ]  && step=200;
    [ $XMAX -gt 5000 ]  && step=500;
    for x in $(seq 0 $step $(( $XMAX + $step )))
    do
      x1=$(bc -l <<<"scale=3;$HXSTART+$x*$HWIDTH/($XMAX+$step)" | sed 's/^\./0\./');
      y1=$HYSTART;
      x2=$x1;
      y2=$(( $HYSTART + $HHEIGTH + 3 ));
      echo -e "  <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\" stroke=\"$HCOLOR\" stroke-width=\"0.1\" stroke-dasharray=\"1 2\" />\n" ;
      y1=$(( $HYSTART - 5 ));
      fs=$(bc -l <<<"scale=3;2*$FONTSIZE/3" | sed 's/^\./0\./');
      echo -e "  <text x=\"$x1\" y=\"$y1\" font-size=\"$fs\" text-anchor=\"middle\">$x</text>\n";
    done
    
    echo -e "  <!-- boxes -->\n" ;

    y=$(( $HYSTART + 1 - 10 ));
    while IFS=$'\t' read -r hex ctg card description
    do
      x=$HXSTART;
      y=$(( $y + 10 ));
      w=$(bc -l <<<"scale=3;$card*$HWIDTH/($XMAX+$step)" | sed 's/^\./0\./');
      h=8;
      echo -e "  <rect x=\"$x\" y=\"$y\" width=\"$w\" height=\"$h\" stroke=\"$HCOLOR\" stroke-width=\"0.1\" fill=\"#$hex\" />\n";
    done < $FTMP ;
      
    echo -e " </g>\n" ;

    echo "</svg>" ; 

  } > $OUTFILE ;


##############################################################################################################
#                                                                                                            #
# MULTIPLE INPUT FILES                                                                                       #
#                                                                                                            #
##############################################################################################################
else
  WMAX=0;
  XMAX=0;
    
  for fin in $FLIST
  do
    $VERBOSE && echo "+ input file:  $fin" ;

    line="$(sed -n '2p;2q' $fin)";
    ncds=$($TAWK '{print$2}' <<<"$line");
    [ $XMAX -lt $ncds ] && XMAX=$ncds;

    name=$($TAWK '{print$1}' <<<"$line");
    line="$(basename ${name%.*})"
    [ $WMAX -lt ${#line} ] && WMAX=${#line};
  done 

  NAMEWIDTH=$(bc <<<"($FONTWIDTH*$WMAX+0.5)/1");
  
  FWIDTH=665;                             # width of the bar graph figure
  FHEIGTH=$(( 15 * $NFILE ));             # heigth of the bar graph figure
  FXSTART=$(( 10 + $NAMEWIDTH + 10 ));    # x start of the bar graph figure
  FYSTART=20;                             # y start of the bar graph figure

  case $FEXT in                           # base color of the histogram
  pdf) FCOLOR="grey"          ;;
  png) FCOLOR="darkslategrey" ;; 
  svg) FCOLOR="darkslategrey" ;;
  esac

  WIDTH=$(( $FXSTART + $FWIDTH + 25 ));   # whole figure dimension
  HEIGTH=$(( 10 + $FHEIGTH + 110 + 10 )); # whole figure dimension

  GSCALE=$(bc -l <<<"scale=3;2000/$WIDTH" | sed 's/^\./0\./');
  GWIDTH=$(bc <<<"($WIDTH*$GSCALE+0.5)/1");
  GHEIGTH=$(bc <<<"($HEIGTH*$GSCALE+0.5)/1");

  { echo -e "<svg version=\"1.1\" width=\"$GWIDTH\" height=\"$GHEIGTH\" xmlns=\"http://www.w3.org/2000/svg\">\n" ;

    echo -e " <style>" ;
    echo -e "  text {" ;
    echo -e "   font-family: Arial, Helvetica, Liberation, sans-serif;" ;
    echo -e "   fill: black;" ;
    echo -e "  }" ;
    echo -e "  line {" ;
    echo -e "   stroke-linecap: round;" ;
    echo -e "  }" ;
    echo -e " </style>\n" ;
  
    echo -e " <g transform=\"scale($GSCALE)\">\n" ;

    echo -e "  <!-- background -->\n" ;

    echo -e "  <rect width=\"$WIDTH\" height=\"$HEIGTH\" fill=\"white\"/>\n" ;

    echo -e "  <!-- y-axis -->\n" ;

    step=2;
    [ $XMAX -gt 10 ]    && step=5;
    [ $XMAX -gt 50 ]    && step=10;
    [ $XMAX -gt 100 ]   && step=20;
    [ $XMAX -gt 500 ]   && step=100;
    [ $XMAX -gt 1000 ]  && step=200;
    [ $XMAX -gt 5000 ]  && step=500;
    for x in $(seq 0 $step $(( $XMAX + $step )))
    do
      x1=$(bc -l <<<"scale=3;$FXSTART+$x*$FWIDTH/($XMAX+$step)" | sed 's/^\./0\./');
      y1=$(( $FYSTART - 1 ));
      x2=$x1;
      y2=$(( $FYSTART + $FHEIGTH + 2 ));
      echo -e "  <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\" stroke=\"$FCOLOR\" stroke-width=\"0.1\" stroke-dasharray=\"1 2\" />\n" ;
      y1=$(( $FYSTART - 5 ));
      fs=$(bc -l <<<"scale=3;5*$FONTSIZE/6" | sed 's/^\./0\./');
      echo -e "  <text x=\"$x1\" y=\"$y1\" font-size=\"$fs\" text-anchor=\"middle\">$x</text>\n";
    done
    
    echo -e "  <!-- bars -->\n" ;

    y=$(( $FYSTART - 15 ));
    for fin in $FLIST
    do
      line="$(sed -n '2p;2q' $fin)";
      ncds=$($TAWK '{print$2}' <<<"$line");
      nrbh=$($TAWK '{print$3}' <<<"$line");
      name="$($TAWK '{print$1}' <<<"$line")";
      name="$(basename ${name%.*})";
      
      y=$(( $y + 15 ));

      # name
      x=$(( $FXSTART - 10 ));
      yt=$(( $y + 10 ));
      echo -e "  <text x=\"$x\" y=\"$yt\" font-size=\"$FONTSIZE\" text-anchor=\"end\">$name</text>\n";

      # CDS
      x=$FXSTART;
      y1=$(( $y + 1 ));
      w=$(bc -l <<<"scale=3;$nrbh*$FWIDTH/($XMAX+$step)" | sed 's/^\./0\./');
      h=2;
      echo -e "  <rect x=\"$x\" y=\"$y1\" width=\"$w\" height=\"$h\" stroke=\"$FCOLOR\" stroke-width=\"0.1\" fill=\"olive\" />\n";
      x=$(bc -l <<<"scale=3;$x+$w" | sed 's/^\./0\./');
      w=$(bc -l <<<"scale=3;($ncds-$nrbh)*$FWIDTH/($XMAX+$step/2)" | sed 's/^\./0\./');
      echo -e "  <rect x=\"$x\" y=\"$y1\" width=\"$w\" height=\"$h\" stroke=\"$FCOLOR\" stroke-width=\"0.1\" fill=\"oldlace\" />\n";

      # COG
      sed -n '5,30p;30q' $fin |
        $TAWK '{print $1"\t"$2}' |
          sed 's/^J\t/FCCCFC\tJ\t/;
               s/^A\t/FCDCFC\tA\t/;
               s/^K\t/FCDCEC\tK\t/;
               s/^L\t/FCDCDC\tL\t/;
               s/^B\t/FCDCCC\tB\t/; 
               s/^D\t/FCFCDC\tD\t/;
               s/^Y\t/FCFCCC\tY\t/;
               s/^V\t/FCFCBC\tV\t/;
               s/^T\t/FCFCAC\tT\t/;
               s/^M\t/ECFCAC\tM\t/;
               s/^N\t/DCFCAC\tN\t/;
               s/^Z\t/CCFCAC\tZ\t/;
               s/^W\t/BCFCAC\tW\t/;
               s/^U\t/ACFCAC\tU\t/;
               s/^O\t/9CFCAC\tO\t/;
               s/^X\t/9CFC9C\tX\t/;
               s/^C\t/BCFCFC\tC\t/;
               s/^G\t/CCFCFC\tG\t/;
               s/^E\t/DCFCFC\tE\t/;
               s/^F\t/DCECFC\tF\t/;
               s/^H\t/DCDCFC\tH\t/;
               s/^I\t/DCCCFC\tI\t/;
               s/^P\t/CCCCFC\tP\t/;
               s/^Q\t/BCCCFC\tQ\t/;
               s/^R\t/E0E0E0\tR\t/;
               s/^S\t/CCCCCC\tS\t/' > $FTMP ;

      x=$FXSTART;
      y2=$(( $y1 + $h ));
      h=11;
      while IFS=$'\t' read -r hex ctg card
      do
        w=$(bc -l <<<"scale=3;$card*$FWIDTH/($XMAX+$step)" | sed 's/^\./0\./');
        echo -e "  <rect x=\"$x\" y=\"$y2\" width=\"$w\" height=\"$h\" stroke=\"$FCOLOR\" stroke-width=\"0.1\" fill=\"#$hex\" />\n";
	x=$(bc -l <<<"scale=3;$x+$w" | sed 's/^\./0\./');
      done < $FTMP 
     
    done 
   
    echo -e "  <!-- bottom caption -->\n" ;

    SQUARE="width=\"8\" height=\"8\" stroke=\"$FCOLOR\" stroke-width=\"0.1\"";
    CAPTION="font-size=\"$FONTSIZE\" text-anchor=\"left\"";
    YS=$(( $FYSTART + $FHEIGTH + 10 ));

    #### Information storage and processing
    x=$(( $WIDTH - 690 )); xt=$(( $x + 12 ));
    y=$YS;            yt=$(( $y + 6 )); hex="#FCCCFC"; line="[J]  Translation, ribosomal structure and biogenesis"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#FCDCFC"; line="[A]  RNA processing and modification"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#FCDCEC"; line="[K]  Transcription"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#FCDCDC"; line="[L]  Replication, recombination and repair"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#FCDCCC"; line="[B]  Chromatin structure and dynamics"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";

    #### CDS with/without assigned COG(s)
    y=$(( $y + 30 )); yt=$(( $y + 6 ));                line="CDS with assigned COG(s)"; 
    echo -e "  <rect x=\"$(( $x + 3 ))\" y=\"$(( $y + 2 ))\" width=\"4\" height=\"4\" stroke=\"$FCOLOR\" stroke-width=\"0.1\" fill=\"olive\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 ));                line="CDS without assigned COG"; 
    echo -e "  <rect x=\"$(( $x + 3 ))\" y=\"$(( $y + 2 ))\" width=\"4\" height=\"4\" stroke=\"$FCOLOR\" stroke-width=\"0.1\" fill=\"oldlace\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";

    ### Cell processing and signaling
    x=$(( $WIDTH - 520 )); xt=$(( $x + 12 ));
    y=$YS;            yt=$(( $y + 6 )); hex="#FCFCDC"; line="[D]  Cell cycle control, cell division,"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
                                                       line="     chromosome partitioning"; 
    echo -e "  <text x=\"$(( $x + 24 ))\" y=\"$(( $y + 14 ))\" $CAPTION >$line</text>\n";
    y=$(( $y + 18 )); yt=$(( $y + 6 )); hex="#FCFCCC"; line="[Y]  Nuclear structure"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#FCFCBC"; line="[V]  Defense mechanisms"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#FCFCAC"; line="[T]  Signal transduction mechanisms"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";

    ### poorly characterized
    y=$(( $y + 30 )); yt=$(( $y + 6 )); hex="#E0E0E0"; line="[R]  General function prediction only"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#CCCCCC"; line="[S]  Function unknown"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";

    ### Cell processing and signaling
    x=$(( $WIDTH - 390 )); xt=$(( $x + 12 ));
    y=$YS;            yt=$(( $y + 6 )); hex="#ECFCAC"; line="[M]  Cell wall/membrane/envelope biogenesis"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" font-size=\"$FONTSIZE\" text-anchor=\"left\">$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#DCFCAC"; line="[N]  Cell motility"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#CCFCAC"; line="[Z]  Cytoskeleton"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#BCFCAC"; line="[W]  Extracellular structures"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#ACFCAC"; line="[U]  Intracellular trafficking, secretion, and vesicular transport"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#9CFCAC"; line="[O]  Posttranslational modification, protein turnover, chaperones"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#9CFC9C"; line="[X]  Mobilome: prophages, transposons"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";

    ### Metabolism
    x=$(( $WIDTH - 180 )); xt=$(( $x + 12 ));
    y=$YS;            yt=$(( $y + 6 )); hex="#BCFCFC"; line="[C]  Energy production and conversion"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#CCFCFC"; line="[G]  Carbohydrate transport and metabolism"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#DCFCFC"; line="[E]  Amino acid transport and metabolism"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#DCECFC"; line="[F]  Nucleotide transport and metabolism"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#DCDCFC"; line="[H]  Coenzyme transport and metabolism"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#DCCCFC"; line="[I]  Lipid transport and metabolism"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#CCCCFC"; line="[P]  Inorganic ion transport and metabolism"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    y=$(( $y + 10 )); yt=$(( $y + 6 )); hex="#BCCCFC"; line="[Q]  Secondary metabolites biosynthesis,"; 
    echo -e "  <rect x=\"$x\" y=\"$y\" $SQUARE fill=\"$hex\" />\n";
    echo -e "  <text x=\"$xt\" y=\"$yt\" $CAPTION >$line</text>\n";
    xt=$(( $x + 24 )); yt=$(( $y + 14 ));              line="     transport and catabolism"; 
    echo -e "  <text x=\"$xt\" y=\"$(( $y + 14 ))\" $CAPTION >$line</text>\n";

    echo -e " </g>\n" ;

    echo "</svg>" ; 

  } > $OUTFILE ;

fi


##############################################################################################################
#                                                                                                            #
# WRITING OUTPUT FILE AND EXIT                                                                               #
#                                                                                                            #
##############################################################################################################

$VERBOSE && echo "> output file: $OUTFILE" ;

# conversion (if any)
case $FEXT in
pdf) mv $OUTFILE $FTMP ; $SVG2PDF -o $OUTFILE $FTMP ;;
png) mv $OUTFILE $FTMP ; $SVG2PNG -o $OUTFILE $FTMP ;;
esac
  
finalize ;

echo "[exit]" ;

exit ;

