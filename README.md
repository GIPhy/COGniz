[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/)
[![Bash](https://img.shields.io/badge/Bash-5.1-be0032)](https://www.gnu.org/software/bash/)


# COGniz

_COGniz_ is a command line tool written in [Bash](https://www.gnu.org/software/bash/) to assign [COG](https://www.ncbi.nlm.nih.gov/research/cog) category(ies) to coding sequences (CDS), the COG ([_Clusters of Orthologous Genes_](https://www.ncbi.nlm.nih.gov/research/cog-project/)) being a well-known databank of amino acid sequence families with associated functional information (e.g. Tatusov et al. 1997, 2003, Galperin et al. 2015, 2021).

_COGniz_ is able to process a bacteria or archaea proteome in less than 2 minutes (using 12 threads), the overall procedure being based on sequence similarity searches using the tool [_DIAMOND_](http://www.diamondsearch.org/) (Buchfink et al. 2021).
Moreover, the accompanying tool _COGfig_ enables to create publication-ready figures from the _COGniz_ results. 

<br>

## Dependencies

You will need to install the required programs and tools listed in the following tables, or to verify that they are already installed with the required version.

##### Mandatory program

<div align="center">

| program                                     | package    | version       | sources                                                               |
|:------------------------------------------- |:----------:| -------------:|:--------------------------------------------------------------------- |
| [_DIAMOND_](http://www.diamondsearch.org/)  | -          | &ge; 2.1.9    | [github.com/bbuchfink/diamond](https://github.com/bbuchfink/diamondo) |

</div>


##### Standard GNU packages and utilities

<div align="center">

| program                                                                           | package                                                 | version      | sources                                                            |
|:--------------------------------------------------------------------------------- |:-------------------------------------------------------:| ------------:|:------------------------------------------------------------------ |
| _echo_ <br> _join_ <br> _mktemp_ <br> _paste_ <br> _printf_ <br> _sort_ <br> _tr_ | [_coreutils_](https://www.gnu.org/software/coreutils/)  | > 8.0        | [ftp.gnu.org/gnu/coreutils](https://ftp.gnu.org/gnu/coreutils)     |
| [_bc_](https://www.gnu.org/software/bc)                                           | -                                                       | > 1.0        | [ftp.gnu.org/gnu/bc](https://ftp.gnu.org/gnu/bc)                   |
| [_gawk_](https://www.gnu.org/software/gawk)                                       | -                                                       | > 4.0.0      | [ftp.gnu.org/gnu/gawk](http://ftp.gnu.org/gnu/gawk/)               |
| [_grep_](https://www.gnu.org/software/grep)                                       | -                                                       | > 2.0        | [ftp.gnu.org/gnu/bc](https://ftp.gnu.org/gnu/grep)                 |
| [_sed_](https://www.gnu.org/software/sed)                                         | -                                                       | > 4.2        | [ftp.gnu.org/gnu/bc](https://ftp.gnu.org/gnu/sed)                  |

</div>

##### Optional programs

<div align="center">

| program                                         | package                                           | version     | sources                                                                    | note                                                                      |
|:----------------------------------------------- |:-------------------------------------------------:| -----------:|:-------------------------------------------------------------------------- |:------------------------------------------------------------------------- |
| _rsvg-convert_                                  | [librsvg](https://gitlab.gnome.org/GNOME/librsvg) | > 2.50      | [gitlab.gnome.org/GNOME/librsvg](https://gitlab.gnome.org/GNOME/librsvg)   | + only to create pdf/png files<br>using _COGfig_                          |
| [_gzip_](https://www.gnu.org/software/gzip/)    | -                                                 | > 1.0       | [ftp.gnu.org/gnu/gzip](https://ftp.gnu.org/gnu/gzip/)                      | + only  when using _makeCOGdb_                                            |
| [_curl_](https://curl.se/)                      | -                                                 | > 7.0       | [github.com/curl/curl](https://github.com/curl/curl)                       | + only  when using _makeCOGdb_ <br>+ either _curl_ or _wget_ are accepted |
| [_wget_](https://www.gnu.org/software/wget/)    | -                                                 | > 1.19      | [ftp.gnu.org/gnu/wget](https://ftp.gnu.org/gnu/wget)                       | + only  when using _makeCOGdb_ <br>+ either _curl_ or _wget_ are accepted |

</div>

<br>

## Installation and execution

### [[ _COGniz_ ]]

**A.** Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/COGniz.git
```

**B.** Go to the created directory and give the execute permission to the file _COGniz.sh_:

```bash
cd COGniz/ 
chmod +x COGniz.sh
```

**C.** Check the dependencies (and their version) using the following command line:

```bash
./COGniz.sh  -c
```

**D.** If at least one of the required program (see [Dependencies](#dependencies)) is not available on your `$PATH` variable (or if one compiled binary has a different default name), it should be manually specified.
To specify the location of a specific binary, edit the file `COGniz.sh` and indicate the local path to the corresponding binary(ies) within the code block `REQUIREMENTS` (approximately lines 100-140).
For each required program, the table below reports the corresponding variable assignment instruction to edit (if needed) within the code block `REQUIREMENTS`

<div align="center">
<sup>

| program          | variable assignment              |   | program          | variable assignment              |
|:---------------- |:-------------------------------- | - |:---------------- |:-------------------------------- |
| _DIAMOND_        | `DIAMOND_BIN=diamond;`           |   | _gawk_           | `GAWK_BIN=gawk;`                 |

</sup>
</div>


**E.** Execute _COGniz_ with the following command line model:

```bash
./COGniz.sh  -i <infile>  [options]
```

<br>

### [[ COGdb ]]

_COGniz_ also requires a databank of COG sequences inside the _db/_ directory, named `COGdb.dmnd`. 

<!-- A version of this databank (3,430,635 sequences, derived from the [2020 update](https://ftp.ncbi.nlm.nih.gov/pub/COG/COG2020/data/); 1.2 Gb, formatted using _DIAMOND_ v2.0.15 in march 2024) can be directly downloaded from the following link:&nbsp;[https://giphy.pasteur.fr/data/COGdb.dmnd](https://giphy.pasteur.fr/data/COGdb.dmnd) -->

A version of this databank (3,430,635 sequences, derived from the [2020 update](https://ftp.ncbi.nlm.nih.gov/pub/COG/COG2020/data/); 1.2 Gb, formatted using _DIAMOND_ v2.1.9 in june 2024) can be directly downloaded from the following link:&nbsp;[https://giphy.pasteur.fr/data/COGdb.dmnd](https://giphy.pasteur.fr/data/COGdb.dmnd)

This databank file can be easily downloaded into the directory _db/_ using e.g. the tool _wget_:

```bash
wget  -O db/COGdb.dmnd  https://giphy.pasteur.fr/data/COGdb.dmnd
```
or the tool _curl_:

```bash
curl  -o db/COGdb.dmnd  https://giphy.pasteur.fr/data/COGdb.dmnd
```

Alternatively, the COG databank `COGdb.dmnd` can be built using the tool _makeCOGdb_ available inside the directory _db/_. 
Just go inside the directory _db/_ and run _makeCOGdb_, e.g.

```bash
cd db/
./makeCOGdb.sh
```

The tool _makeCOGdb_ will download every COG file (using either _wget_ or _curl_) from the [NCBI repository](https://ftp.ncbi.nlm.nih.gov/pub/COG/COG2020/data/fasta/), and next extract the sequence regions associated to each COG. 
Finally, _makeCOGdb_ will format the created sequence file using _DIAMOND_. 
The overall running time is expected to be &lt; 20 minutes with the default number of threads (i.e. 6); however, faster running times can be observed using a larger number of threads that can be set as a parameter, e.g. 

```bash
./makeCOGdb.sh 48
```

<br>

### [[ _COGfig_ ]]

The tool _COGfig_ is expected to run without problem to create [SVG](https://developer.mozilla.org/en-US/docs/Web/SVG) files when the dependencies required by _COGniz_ are installed.

Just give the execute permission to the file `COGfig.sh`:

```bash
chmod +x COGfig.sh
```

However, to create [PDF](https://www.adobe.com/acrobat/about-adobe-pdf.html) or [PNG](https://www.w3.org/TR/PNG/) files, the tool _rsvg-convert_ (from [librsvg](https://gitlab.gnome.org/GNOME/librsvg)) is also required, as well as at least one of the following fonts: Arial, Helvetica, Liberation (run the tool `fc-list` to check the available fonts).
If _rsvg-convert_ is not available on your `$PATH` variable, it should be manually specified within the code block `REQUIREMENTS` (approximately lines 70-100) at `RSVGCONVERT_BIN=rsvg-convert;`.

Note that other tools can be used to edit and/or convert the default [SVG](https://developer.mozilla.org/en-US/docs/Web/SVG)-formatted files created by _COGfig_, e.g. [_Inkscape_](https://inkscape.org/).


<br>

## Usage

### [[ _COGniz_ ]]

Run _COGniz_ without option to read the following documentation:

```
 USAGE:  COGniz  -i <infile>   [-d <COGdb>]     [-o <outfile>]  [-O <outfile>] 
                [-w <tmpdir>]  [-t <nthreads>]  [-v]  [-c]  [-h]
 OPTIONS:
  -i <file>    input file name in FASTA or GenBank format (mandatory)
  -d <file>    COG databank file (default: db/COGdb.dmnd in the same directory as COGniz)
  -o <string>  output file name to write COG assignment results (default: none)
  -O <string>  output file name to  write the input file  completed with  COG assignments
               (default: none)
  -w <dir>     path to the tmp directory (default: $TMPDIR, otherwise /tmp)
  -t <int>     thread numbers (default: 2)
  -v           verbose mode
  -c           checks dependencies and exit
  -h           prints this help and exit
```

### [[ _COGfig_ ]]

Run _COGfig_ without option to read the following documentation:

```
 USAGE:  COGfig  [options]  <infile>  [<infile> ...]

 OPTIONS:
  -o <string>  output file name; supported format: png, pdf, svg (default: COGfig.svg)
  -a           sort input files (ascending order of the no. assigned COGs)
  -d           sort input files (descending order of the no. assigned COGs)
  -v           verbose mode
  -h           prints this help and exit
```

<br>

## Notes

* In brief, the CDS gathered from the input file are used as queries to perform a first sequence similarity search against the COG databank using _DIAMOND_ (default scoring parameters; multiple rounds of searches with increasing sensitivity using option `--iterate`). <br>Each query CDS is assigned with the COG family of the best hit (BH) sequence when the 10 first BHs: <br>&emsp;(i) are likely homologous sequences (assessed with a local alignment score &gt;&nbsp;60 bits; Pearson 2013), and <br>&emsp;(ii) belong to the same COG family.<br>For the remaining input CDS, the homology with their respective first BH COG sequence is assessed using a reciprocal best hit (RBH) approach.

* The input file (mandatory option `-i`) can be in FASTA or [GenBank](https://www.ncbi.nlm.nih.gov/genbank/samplerecord/) format. FASTA-formatted file should contain amino acid sequences, whereas GenBank-formatted file should contain translated amino acid sequences (feature `/translation=`) associated with a locus tag (feature `/locus_tag=`) or a protein id (feature `/protein_id=`).

* By default, _COGniz_ expects that the COG databank file `COGdb.dmnd` is located in the directory _db/_. However, an alternative COG databank file (e.g. different version, different file name) can be specified using option `-d`.

* Fast running times can be obtained using multiple threads (option `-t`) and/or a temporary directory located on a hard drive with high speed (option `-w`).

* The COG assignation results can be written into a tab-delimited output file (option `-o`). The input file can be completed with the COG assignation results (option `-O`), as a header suffix (FASTA format) or a feature `/note=` (GenBank format).

* The tab-delimited result file written by _COGniz_ (option `-o`) can be used as an input file by _COGfig_ to create a barchart figure of the different assigned COG categories (see example below). _COGfig_ can also summarize different _COGniz_ result files into a single figure, with sorted representation (options `-a` or `-d`) or using the input file order (default).


<br>

## Example

The following example describes the usage of _COGniz_ to assign COG categories to the proteome of _Escherichia_ type strains (_E.&nbsp;albertii_, _E.&nbsp;coli_, _E.&nbsp;fergusonii_, _E.&nbsp;marmotae_, _E.&nbsp;whittamii_), as well as the usage of _COGfig_ to build associated figures.

**Downloading input files**

The GenBank-formatted files (.gbf) of the five annotated genomes can be downloaded using the following command lines:

```bash
NCBIFTP="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF";
wget -q -O - $NCBIFTP/020/097/475/GCF_020097475.1_ASM2009747v1/GCF_020097475.1_ASM2009747v1_genomic.gbff.gz | gunzip -c > E.fergusonii.ATCC35469.gbf
wget -q -O - $NCBIFTP/003/697/165/GCF_003697165.2_ASM369716v2/GCF_003697165.2_ASM369716v2_genomic.gbff.gz   | gunzip -c > E.coli.ATCC11775.gbf
wget -q -O - $NCBIFTP/022/833/075/GCF_022833075.1_EaTS/GCF_022833075.1_EaTS_genomic.gbff.gz                 | gunzip -c > E.albertii.DSM17582.gbf
wget -q -O - $NCBIFTP/002/900/365/GCF_002900365.1_ASM290036v1/GCF_002900365.1_ASM290036v1_genomic.gbff.gz   | gunzip -c > E.marmotae.HT073016.gbf
wget -q -O - $NCBIFTP/014/836/715/GCF_014836715.1_ASM1483671v1/GCF_014836715.1_ASM1483671v1_genomic.gbff.gz | gunzip -c > E.whittamii.Sa2BVA5.gbf
```

**Running _COGniz_**

After verifying that the COG databank _COGdb.dmnd_ is present in the directory _db/_, use the following command line to run _COGniz_ on the _E. coli_ GenBank file using 12 threads:

```bash
./COGniz.sh -i E.coli.ATCC11775.gbf -v -t 12 -o E.coli.ATCC11775.tsv -O E.coli.ATCC11775.gbk
```

As the verbose mode was set (`-v`), this leads to the following output:

```
# COGniz v1.1
# Copyright (C) 2024 Institut Pasteur
+ https://gitlab.pasteur.fr/GIPhy/COGniz
> Syst:  x86_64-redhat-linux-gnu
> Bash:  4.4.20(1)-release
> COGdb: /local/bin/COGniz/db/COGdb.dmnd
[00:01] creating tmp directory ... [ok]
> TMP_DIR=/tmp/COGniz.p2mOrROvTk
[00:01] reading infile ... [ok]
> file name:       E.coli.ATCC11775.gbf
> detected format: GenBank (with protein ids)
> no. CDS:         4619
[00:01] searching COG databank ...... [ok]
> no. assigned CDS:   1519
> no. unassigned CDS: 3100
[00:46] reciprocal searching ... [ok]
> no. RBHs:           2192
> no. assigned CDS:   3711
> no. unassigned CDS: 908

ctg  no.    description
---  ---    -------------------------------------------------------------
J    263    Translation, ribosomal structure and biogenesis
A    2      RNA processing and modification
K    314    Transcription
L    155    Replication, recombination and repair
B    0      Chromatin structure and dynamics
D    49     Cell cycle control, cell division, chromosome partitioning
Y    0      Nuclear structure
V    116    Defense mechanisms
T    212    Signal transduction mechanisms
M    301    Cell wall/membrane/envelope biogenesis
N    100    Cell motility
Z    2      Cytoskeleton
W    39     Extracellular structures
U    106    Intracellular trafficking, secretion, and vesicular transport
O    173    Posttranslational modification, protein turnover, chaperones
X    77     Mobilome: prophages, transposons
C    291    Energy production and conversion
G    443    Carbohydrate transport and metabolism
E    380    Amino acid transport and metabolism
F    119    Nucleotide transport and metabolism
H    213    Coenzyme transport and metabolism
I    128    Lipid transport and metabolism
P    250    Inorganic ion transport and metabolism
Q    52     Secondary metabolites biosynthesis, transport and catabolism
R    233    General function prediction only
S    162    Function unknown
-    908    CDS without assigned COG


[00:47] writing output file(s) ... [ok]
+ COG assignments: E.coli.ATCC11775.tsv
+ sequence file:   E.coli.ATCC11775.gbk
[00:47] exit
```

From the 4,619 _E. coli_ CDS, _COGniz_ therefore produced 3,711 ones with assigned COG family (and associated categories) in less than one minute.
Result files (_E.coli.ATCC11775.tsv_ and _E.coli.ATCC11775.gbk_) are available in the directory _example/_.

Next, use the following command line to run _COGfig_ on the tab-delimited result file _E.coli.ATCC11775.tsv_:

```bash
./COGfig.sh -o E.coli.ATCC11775.svg  E.coli.ATCC11775.tsv
```

to obtain the following graphical representation of the COG category distribution:

<p align="center">
  <img width="80%" height="auto" align="center" src="example/E.coli.ATCC11775.svg">
</p>


The remaining four GenBank files can be processed by _COGniz_ using similar command lines, e.g.

```bash
./COGniz.sh -i E.albertii.DSM17582.gbf    -v -t 12 -o E.albertii.DSM17582.tsv
./COGniz.sh -i E.fergusonii.ATCC35469.gbf -v -t 12 -o E.fergusonii.ATCC35469.tsv
./COGniz.sh -i E.marmotae.HT073016.gbf    -v -t 12 -o E.marmotae.HT073016.tsv
./COGniz.sh -i E.whittamii.Sa2BVA5.gbf    -v -t 12 -o E.whittamii.Sa2BVA5.tsv
```

and a graphical representation of the five analyses can be obtained using the following command line:

```bash
./COGfig.sh -a -o Escherichia.svg  E.*.tsv
```

<p align="center">
  <img width="100%" height="auto" align="center" src="example/Escherichia.svg">
</p>

Of note, the number of assigned CDS is always lower than the total number of COG functional categories, because some COG families are associated to more than one categories.


<br>

## References

Buchfink B, Reuter K, Drost HG (2021) _Sensitive protein alignments at tree-of-life scale using DIAMOND._ **Nature Methods** 18:366-368. [doi:10.1038/s41592-021-01101-x](https://doi.org/10.1038/s41592-021-01101-x).

Galperin MY, Makarova KS, Wolf YI, Koonin EV (2015) _Expanded microbial genome coverage and improved protein family annotation in the COG database._ **Nucleic Acids Research**, 43(Database issue):D261-D269. [doi:10.1093/nar/gku1223](https://doi.org/10.1093/nar/gku1223).

Galperin MY, Wolf YI, Makarova KS, Alvarez RV, Landsman D, Koonin EV (2021) _COG database update: focus on microbial diversity, model organisms, and widespread pathogens._ **Nucleic Acids Research**, 49(D1):D274-D281. [doi:10.1093/nar/gkaa1018](https://doi.org/10.1093/nar/gkaa1018).

Pearson WR (2013) _An Introduction to Sequence Similarity (“Homology”) Searching._ **Current Protocols in Bioinformatics**, 42:3.1.1-3.1.8. [doi:10.1002/0471250953.bi0301s42](https://doi.org/10.1002/0471250953.bi0301s42). 

Tatusov RL, Fedorova ND, Jackson JD, Jacobs AR, Kiryutin B, Koonin EV, Krylov DM, Mazumder R, Mekhedov SL, Nikolskaya AN, Rao BS, Smirnov S, Sverdlov AV, Vasudevan S, Wolf YI, Yin JJ, Natale DA (2003) _The COG database: an updated version includes eukaryotes._ **BMC Bioinformatics**, 4:41. [doi:10.1186/1471-2105-4-41](https://doi.org/10.1186/1471-2105-4-41).

Tatusov RL, Koonin EV, Lipman DJ (1997) _A genomic perspective on protein families._ **Science**, 278(5338):631-637. [doi:10.1126/science.278.5338.631](https://doi.org/10.1126/science.278.5338.631).


## Citations

Kämpfer P, Glaeser SP, McInroy JA, Busse H-J, Clermont D, Criscuolo A (2024)
_Description of Cohnella rhizoplanae sp. nov., isolated from the root surface of soybean (Glycine max)_.
**Antonie van Leeuwenhoek**, 118:41.
[doi:10.1007/s10482-024-02051-y](https://doi.org/10.1007/s10482-024-02051-y)

Saraf A, Blondet E, Boullié A, Criscuolo A, Gugger M (2025)
_Insight on the heterocyte patterning and the proheterocyte division in the toxic cyanobacterium Kaarinaea lacus gen. nov., sp. nov., and its genomic potential for natural products_.
**Harmful Algae**, 142:102792.
[doi:10.1016/j.hal.2024.102792](https://doi.org/10.1016/j.hal.2024.102792)


